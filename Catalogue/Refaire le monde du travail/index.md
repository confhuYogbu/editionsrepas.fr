---
Edition: Editions REPAS
DépotLégal: 2016
Auteur: Sandrino Graceffa, en collaboration avec Roger Burton, Virginie Cordier et Carmelo Virone
Titre: Refaire le Monde... du Travail
SousTitre: Une alternative à l'ubérisation de l'économie
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-11-2
Pages: 112 pages
Prix: 10
Etat: Indisponible
Résumé: |
    L’intérêt que suscite la question du « travail » est unique aujourd’hui en France et dans l’ensemble du monde occidental. Le contrat social du salariat basé sur la subordination en échange de protection a-t-il un avenir ? L’emploi salarié et la protection sociale résisteront-ils aux multiples tentatives à l’œuvre visant toujours plus de flexibilité et moins de sécurité ? Est-il vraiment indispensable que tous les travailleurs deviennent des entrepreneurs d’eux-mêmes ?

    Alors que le spectre d’une ubérisation croissante du monde du travail apparaît pour certains comme une fatalité, des voix s’élèvent pour expérimenter des alternatives crédibles, notamment au travers du mouvement des communs. Les nouvelles formes de coopératives de travail ouvertes peuvent-elles s’imposer comme un véritable modèle de préfiguration d’une société post-capitaliste ?

    Fort de ses expériences de terrain au sein de plusieurs coopératives d'emploi, l'auteur propose une analyse de l’évolution du monde du travail et des pistes sérieuses permettant d’accompagner les changements à l’œuvre dans une perspective de progrès social.
Tags:
- Travail
- Alternatives
- Collectif
- Economie sociale et solidaire
- Coopératives d'Activité et d'Emploi
SiteWeb: https://www.smartfr.fr/
CommandeWeb: https://www.ardelaine.fr/librairie-pratiques-alternatives-solidaires/69-livre-refaire-le-monde-du-travail-edrepas-alternatives-et-solidaires.html
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272112-refaire-le-monde-du-travail-une-alternative-a-l-uberisation-de-l-economie-sandrino-graceffa/
Couverture:
Couleurs:
 PageTitres: '#f0000a'
 Texte: '#ffffff'
 Titre: '#ffffff'
Bandeau: Epuisé
Adresse: Rue Coenraets 72, 1060 Saint-Gilles, Belgique
Latitude: 50.8320172
Longitude: 4.3360868
---

## Les auteurs

Entrepreneur social et créateur d’initiatives locales, **Sandrino Graceffa** a occupé plusieurs postes à responsabilité au sein d’associations, de collectivités et d’entreprises.

Spécialiste des nouvelles formes de coopérations entre acteurs économiques, il dirige SMart depuis 2014. Cette coopérative de travailleurs accompagne depuis 1998 quelque 75.000 porteurs de projets dans la gestion de leurs activités professionnelles à travers 32 bureaux dans 9 pays d’Europe, en leur proposant une solution originale pour créer leur emploi salarié en développant leur propre activité économique.

*Livre rédigé en collaboration avec **Roger Burton, Virginie Cordier et Carmelo Virone**.*


## Extraits

> Si nous parvenons à offrir un maximum de protection sociale à un maximum de personnes, nous sommes gagnants. Il faut qu’on puisse passer d’un travail indépendant à un travail salarié, sans que le changement porte à conséquence sur les droits que l’on a ouverts, que ce soit en matière de sécurité sociale, de chômage ou de retraite.  (...)
> Le travail aujourd’hui devient de plus en plus autonome. Mais plutôt que de créer sa propre entreprise et d’en assurer seul les risques, on peut s’inscrire dans une logique d’entrepreneuriat collectif, en créant son emploi salarié dans une entreprise coopérative partagée. C’est cette voie que nous voulons privilégier.
> -- pages 41-43

> La question des relations sociales s’est imposée comme un élément très important de la réussite des projets, tout comme de leur fragilisation en creux. Les personnes s’asséchaient socialement en devenant entrepreneurs individuels, alors que l’acte d’entreprise individuelle nécessite plutôt qu’on élargisse son cercle de connaissances et de solidarité.
> Il fallait trouver autre chose, inventer d’autres formes d’entrepreneuriats qui permettent aux gens de se regrouper. C’est à cette fin que nous avons lancé en 2006 Grands Ensemble, la première coopérative d’activités dans le Pas-de-Calais 21. J’y ai retrouvé le même public que celui que j’avais laissé quelques années auparavant dans l’ex-bassin minier, un public de gens qui voulaient créer leur propre activité. C’était vraiment passionnant de s’apercevoir que les personnes développaient leur propre activité à l’intérieur d’une entreprise collective et que, du coup, elles avaient, entre porteuses de projets, le sentiment d’être des collègues de travail et, comme on l’est dans le monde ouvrier, se retrouvaient nécessairement solidaires.
> -- page 61
